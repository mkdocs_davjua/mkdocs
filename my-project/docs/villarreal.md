#Villarreal#

La revista oficial del Sevilla 'Football Club' ha publicado una extensa entrevista con un Jorge Sampaoli muy cercano, que cuenta desde las dificultades que tuvo en sus inicios para hacerse un hueco en el mundo del fútbol desde su Casilda natal hasta el "complicado" asunto de traer al equipo de Nervión una forma de juego "totalmente opuesta" a la que practicaba con Unai Emery.
