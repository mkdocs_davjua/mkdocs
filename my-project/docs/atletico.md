#Atletico de madrid#

Álvaro Medrán no podrá jugar el domingo frente al Atlético de Madrid. El centrocampista cordobés sufre un leve esguince en el tobillo izquierdo y se esperará a su evolución para comprobar el tiempo definitivo de baja.

Medrán sufrió un golpe en el entrenamiento del miércoles por la mañana. Tras las pruebas se descartó que fuera algo más grave. Hay que recordar que el canterano del Real Madrid ha sido operado en dos ocasiones del peroné del otro tobillo.

Por tanto, junto con el argentino Garay, que hoy ha vuelto a trabajar en solitario, son las únicas dos bajas que Voro tiene para el partido del domingo.
