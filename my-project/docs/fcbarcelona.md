F.C.Barcelona
===================


El Fútbol Club Barcelona es una entidad polideportiva de Barcelona (España). Fue fundado como club de fútbol el 29 de noviembre de 1899.

----------


Plantilla
-------------

 1. Ter Stegen
 3. Gerard Pique
 4. Rakitik
 5. Busquets
 6. Denis Suarez
 7. Arda
 8. Iniesta
 9. Suarez
 10. Messi
 11. Neymar
 12. Rafinha
 13. Cillessen
 14. Mascherano 
 17. Paco Alcacer
 18. Jordi Alba
 19. Digne
 20. Sergi Roberto
 21. Andre Gomes
 22. Aleix Vidal
 23. Umtiti
 24. Mathieu
 25. Masip

![Con titulo](img/barcelona.png "FCBarcelona")
