#Real Madrid#

There's a single configuration file named mkdocs.yml, and a folder named docs that will contain our documentation source files. Right now the docs folder just contains a single documentation page, named index.md.

MkDocs comes with a built-in webserver that lets you preview your documentation as you work on it. We start the webserver by making sure we're in the same directory as the mkdocs.yml config file, and then running the mkdocs serve command:
