#Sevilla#

El defensa de la Real Sociedad Héctor Hernández, titular tras la lesión de Yuri Berchiche, ha dicho hoy que el equipo ya ha olvidado la derrota en el derbi guipuzcoano contra el Eibar aunque admite que tienen "muchas cosas que mejorar tras la derrota del sábado".

El jugador vallisoletano tendrá otras dos o tres semanas para hacerse con el puesto porque Yuri sufrió una rotura fibrilar que le obligará a causar baja hasta mediados de octubre y seguramente tendrá una plaza segura este viernes en el partido contra el conjunto sevillano.
